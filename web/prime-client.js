const LIST_PRIMES_ID = "list-primes-at-most-limit";
const IS_PRIME_ID = "is-input-prime";

document.getElementById(IS_PRIME_ID).addEventListener("submit", function(event) {
    handleRequest(event, IS_PRIME_ID);
});

document.getElementById(LIST_PRIMES_ID).addEventListener("submit", function(event) {
    handleRequest(event, LIST_PRIMES_ID);
});

function handleRequest(event, formId) {
    const request = new XMLHttpRequest();
    const form = document.getElementById(formId);
    const answer = document.getElementById(formId + '-answer');
    answer.className = null;
    answer.innerHTML = "Loading...";
    request.open('GET', "api/" + formId + "/" + encodeURI(form.elements["input"].value));
    request.onload = function() {
        if (request.status === 200) {
            if (formId === IS_PRIME_ID) {
                if (JSON.parse(this.response)) {
                    answer.className = "prime";
                    answer.innerHTML = "Yes!";
                } else {
                    answer.className = "nonprime";
                    answer.innerHTML = "No";
                }
            } else if (formId === LIST_PRIMES_ID) {
                const jsonResponse = JSON.parse(this.response);
                if (!jsonResponse.length) {
                    answer.className = "nonprime";
                    answer.innerHTML = "None!"
                } else {
                    answer.className = "prime";
                    answer.innerHTML = jsonResponse.join(", ");
                }
            }
        } else if (request.status === 400) {
            answer.className = null;
            answer.innerHTML = JSON.parse(this.response).message;
        } else if (request.status === 504) {
            answer.className = null;
            answer.innerHTML = "Took too long to process. Try a smaller number.";
        } else {
            answer.className = null;
            answer.innerHTML = "Something went wrong. Try again later!"
        }
    };
    request.send();
    // we still want forms organization but we don't want submit to reload the page
    event.preventDefault()
}