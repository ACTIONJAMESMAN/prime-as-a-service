# Prime as a Service

This service provides a few operations that help you with many of your prime number needs.

The following API calls are available:

#### **GET** `/prime-as-a-service/api/is-input-prime/{input}`

Where `{input}` is a 32-bit integer, return `true` if it is a prime number and `false` if not.

#### **GET** `/prime-as-a-service/api/list-primes-under-limit/{limit}`

Where `{limit}` is a 32-bit integer, return a list of all prime numbers under or matching the limit.

##### Front End

You may also access these endpoints through a web UI located at `/prime-as-a-service/`

## Installation

#### Prerequisites 

Start by cloning this repo in any location on your web server:

```bash
$ git clone https://gitlab.com/ACTIONJAMESMAN/prime-as-a-service.git
```

#### Java Service

To build the service:

```bash
$ ./gradlew clean build
```

Execute the `.jar` to begin the service:

```bash
$ java -jar build/libs/prime-as-a-service-1.0.0.jar
```

Or, to daemonize the `.jar`:
 
1. Copy `build/libs/prime-as-a-service-1.0.0.jar` to `/opt/prime-as-a-service/`
2. Copy `prime-as-a-service.service` to `/etc/systemd/system/` 
3. Run the following:
   ```bash
   $ sudo systemctl daemon-reload
   $ sudo systemctl start prime-as-a-service
   ```

#### Serving everything through Nginx

Copy the `web` directory and `nginx.conf` to `/opt/prime-as-a-service/`

Add the following line under the server directive of your choosing in your Nginx config:

```nginx
include /opt/prime-as-a-service/nginx.conf;
```

This config blocks the API and UI behind basic auth. Run the following to create a credentials file:

```bash
$ sudo htpasswd -c /opt/prime-as-a-service/.htpasswd [USERNAME]
```

Reload Nginx and all services should be available.