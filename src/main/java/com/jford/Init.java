package com.jford;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Init {

    /**
     * Entry point into the Spring application
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(Init.class, args);
    }

}