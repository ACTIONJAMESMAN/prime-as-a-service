package com.jford;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigInteger;
import java.util.List;

@RestController
@RequestMapping("/")
public class PrimeController {

    private PrimeService primeService;

    @Autowired
    public PrimeController(PrimeService primeService) {
        this.primeService = primeService;
    }

    @RequestMapping("/is-input-prime/{input}")
    public boolean isInputPrime(@PathVariable String input) {
        return primeService.isInputPrime(validateAndConvertToInteger(input));
    }

    @RequestMapping("/list-primes-at-most-limit/{limit}")
    public List<Integer> listPrimesAtMostLimit(@PathVariable String limit) {
        return primeService.listPrimesAtMostLimit(validateAndConvertToInteger(limit));
    }

    private int validateAndConvertToInteger(String input) {
        try {
            BigInteger numberValidation = new BigInteger(input);
            if (numberValidation.compareTo(BigInteger.valueOf(Integer.MAX_VALUE)) > 0) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        "Values above " + Integer.MAX_VALUE + " are not supported");
            }
            return numberValidation.intValue();
        } catch (NumberFormatException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Input " + input + " is not an integer");
        }
    }
}
