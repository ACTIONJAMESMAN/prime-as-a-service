package com.jford;

import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Component
public class PrimeService {

    /**
     * Determines whether or not the input is a prime number
     * @param input any number
     * @return true if input is prime, false if not
     */
    public boolean isInputPrime(int input) {
        // only check the square root of the input (rounded up) to optimize this call
        List<Integer> primesUnderSquareRoot = listPrimesAtMostLimit((int) Math.ceil(Math.sqrt(input)));
        return !CollectionUtils.isEmpty(primesUnderSquareRoot) && !isNumberDivisibleByList(input, primesUnderSquareRoot);
    }

    /**
     * Finds all prime numbers that are AT MOST the limit, AKA all numbers less than or equal to the limit
     * @param limit the upper limit to stop searching for prime numbers
     * @return a list of prime numbers less than or equal to the limit
     */
    public List<Integer> listPrimesAtMostLimit(int limit) {
        final int SMALLEST_PRIME = 2;

        List<Integer> primeList = new ArrayList<>();
        if (SMALLEST_PRIME <= limit) {
            primeList.add(SMALLEST_PRIME);
            // start just above SMALLEST_PRIME, then increment by SMALLEST_PRIME to ensure index is indivisible by it (odd)
            for (int index = SMALLEST_PRIME + 1; index <= limit; index += SMALLEST_PRIME) {
                if (!isNumberDivisibleByList(index, primeList)) {
                    primeList.add(index);
                }
            }
        }
        return primeList;
    }

    /**
     * This iterates through a list of prime numbers to determine if the number is divisible by any of them
     * @param input any number
     * @param primeNumberList an ordered list of prime numbers
     * @return true if input is divisible by primeNumberList, false if not
     */
    private boolean isNumberDivisibleByList(int input, List<Integer> primeNumberList) {
        for (Integer prime : primeNumberList) {
            if (input <= prime) {
                break;
            }
            if (input % prime == 0) {
                return true;
            }
        }
        return false;
    }

}
