package com.jford;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class PrimeControllerTest {
    private final int PRIME_NUMBER = 11;
    private final String NON_NUMERIC_STRING = "This is a non-numeric string";
    private final String NUMERIC_STRING_ABOVE_MAX_INT =
            String.valueOf((BigInteger.valueOf(Integer.MAX_VALUE)).add(BigInteger.ONE));

    @Mock
    private PrimeService mockPrimeService;

    private PrimeController primeController;

    @Before
    public void setup() {
        initMocks(this);
        primeController = new PrimeController(mockPrimeService);
    }

    @Test(expected = ResponseStatusException.class)
    public void isInputPrime_givenNonNumericString_throwsError() {
        primeController.isInputPrime(NON_NUMERIC_STRING);
    }

    @Test(expected = ResponseStatusException.class)
    public void isInputPrime_givenNumericStringAboveMaxInt_throwsError() {
        primeController.isInputPrime(NUMERIC_STRING_ABOVE_MAX_INT);
    }

    @Test
    public void isInputPrime_givenValidNumber_returnCorrectResponse() {
        when(mockPrimeService.isInputPrime(PRIME_NUMBER)).thenReturn(true);
        assert(primeController.isInputPrime(String.valueOf(PRIME_NUMBER)));
    }

    @Test(expected = ResponseStatusException.class)
    public void listPrimesAtMostLimit_givenNonNumericString_throwsError() {
        primeController.listPrimesAtMostLimit(NON_NUMERIC_STRING);
    }

    @Test(expected = ResponseStatusException.class)
    public void listPrimesAtMostLimit_givenNumericStringAboveMaxInt_throwsError() {
        primeController.listPrimesAtMostLimit(NUMERIC_STRING_ABOVE_MAX_INT);
    }

    @Test
    public void listPrimesAtMostLimit_givenValidNumber_returnCorrectResponse() {
        final List<Integer> PRIMES_UNDER_PRIME_NUMBER = Arrays.asList(2, 3, 5, 7, 11);
        when(mockPrimeService.listPrimesAtMostLimit(PRIME_NUMBER)).thenReturn(PRIMES_UNDER_PRIME_NUMBER);
        assertEquals(PRIMES_UNDER_PRIME_NUMBER, primeController.listPrimesAtMostLimit(String.valueOf(PRIME_NUMBER)));
    }
}
