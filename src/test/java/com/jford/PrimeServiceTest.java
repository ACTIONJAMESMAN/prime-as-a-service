package com.jford;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class PrimeServiceTest {
    private final List<Integer> LIST_OF_PRIMES = Arrays.asList(2, 3, 5, 7);

    private PrimeService primeService = new PrimeService();

    @Test
    public void listPrimesAtMostLimit_givenLimit_returnsPrimeList() {
        final int LIMIT = 10;
        assertEquals(LIST_OF_PRIMES, primeService.listPrimesAtMostLimit(LIMIT));
    }

    @Test
    public void listPrimesAtMostLimit_givenSmallestPrime_returnsItself() {
        final int LIMIT = 2;
        final List<Integer> EXPECTED = Collections.singletonList(LIMIT);
        assertEquals(EXPECTED, primeService.listPrimesAtMostLimit(LIMIT));
    }

    @Test
    public void listPrimesAtMostLimit_givenZeroPrime_returnsEmptyList() {
        final int LIMIT = 0;
        final List<Integer> EXPECTED = Collections.EMPTY_LIST;
        assertEquals(EXPECTED, primeService.listPrimesAtMostLimit(LIMIT));
    }

    @Test
    public void isInputPrime_givenPrimeNumbers_returnTrueForAll() {
        for (int primeNumber : LIST_OF_PRIMES) {
            System.out.println("Testing for " + primeNumber);
            assert(primeService.isInputPrime(primeNumber));
        }
    }

    @Test
    public void isInputPrime_givenCompositeNumber_returnFalse() {
        final int COMPOSITE_NUMBER = 10;
        assert(!primeService.isInputPrime(COMPOSITE_NUMBER));
    }

    @Test
    public void isInputPrime_givenNegativeNumber_returnFalse() {
        final int NEGATIVE_NUMBER = -5;
        assert(!primeService.isInputPrime(NEGATIVE_NUMBER));
    }

    @Test(timeout=1000)
    public void isInputPrime_givenLargestInt_returnTrueUnderOneSecond() {
        assert(primeService.isInputPrime(Integer.MAX_VALUE));
    }
}
